<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('party', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('hostName');
            $table->string('hostLastName');
            $table->string('country');
            $table->string('city');
            $table->string('address');
            $table->string('postCode');
            $table->string('partyDate');
            $table->string('time');
            $table->string('duration');
            $table->string('hostEmail')->nullable();
            $table->string('hostPhone');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('party');
    }
}
