<?php

namespace Tests\IoC;

use SharedKernel\Container\IoC;
use Tests\TestCase;

class Foo{}

class IoCTest extends TestCase
{
    /** @test
     * @throws \Exception
     */
    public function can_resolve_out_of_the_ioc_container()
    {
        IoC::bind('foo', Foo::class);

        $this->assertInstanceOf(Foo::class, IoC::make('foo'));
    }
}

