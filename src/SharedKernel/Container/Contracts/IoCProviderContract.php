<?php


namespace SharedKernel\Container\Contracts;


interface IoCProviderContract
{
    public function register(Object $abstract);
}
