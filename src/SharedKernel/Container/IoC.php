<?php

namespace SharedKernel\Container;

use Exception;

class IoC
{
    /**
     * @var array
     */
    protected static $registry = [];

    /**
     * @param $abstract
     * @param $concrete
     */
    public static function bind($abstract, $concrete)
    {
        static::$registry[$abstract] = $concrete;
    }

    /**
     * @param $abstract
     * @return mixed
     * @throws Exception
     */
    public static function make($abstract)
    {
        if (isset(static::$registry[$abstract]))
        {
            $resolver = static::$registry[$abstract];

            return new $resolver();
        }
        throw new Exception('Alias does not exist in IoC registry');
    }
}

