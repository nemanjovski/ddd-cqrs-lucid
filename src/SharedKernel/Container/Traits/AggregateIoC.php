<?php

namespace SharedKernel\Container\Traits;

use Exception;
use SharedKernel\Container\IoC;

trait AggregateIoC
{
    public function build(array $bindings, Object $abstract)
    {
        if(array_key_exists(get_class($abstract), $bindings)) {

                IoC::bind(get_class($abstract), $bindings[get_class($abstract)]);

                return IoC::make(get_class($abstract));
        }

        throw new Exception('No bent classes found in IoC container.');
    }
}

