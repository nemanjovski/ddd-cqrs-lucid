<?php

namespace SharedKernel;

use SharedKernel\Foundation;
use Illuminate\Foundation\Application;
use SharedKernel\Application\AbstractApplicationServiceProvider;
use SharedKernel\Foundation\Gateways\SymfonyMessengerQueryGateway;

/**
 * Class SharedKernelServiceProvider
 * @package SharedKernel
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
class SharedKernelServiceProvider extends AbstractApplicationServiceProvider
{
    /** @var array */
    protected $queries = [];

    private $binds = [];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->binds as $contractName => $implName) {
            $this->app->bind($contractName, $implName);
        }

        $this->app->singleton(Foundation\Bus\Router\CommandRouter::class, Foundation\Messenger\Router\CommandRouter::class);
        $this->app->singleton(Foundation\Bus\Router\QueryRouter::class, Foundation\Messenger\Router\CommandRouter::class);

        $this->app->singleton(Foundation\Messenger\MessageBus::class);
        $this->app->singleton(Foundation\Messenger\Middleware\HandleMessageWithPromiseMiddleware::class);

        $this->app->singleton('shared.kernel.handlers.locator.query', function (Application $app) {
            return new Foundation\Messenger\Handler\ContainerAwareHandlersLocator($app, $app->make(Foundation\Bus\Router\QueryRouter::class));
        });

        $this->app->singleton('shared.kernel.handlers.locator.command', function (Application $app) {
            return new Foundation\Messenger\Handler\ContainerAwareHandlersLocator($app, $app->make(Foundation\Bus\Router\CommandRouter::class));
        });

        $this->app->singleton(Foundation\Messenger\SymfonyQueryBus::class, function (Application $app) {
            $bus = new Foundation\Messenger\MessageBus([
                new Foundation\Messenger\Middleware\HandleMessageWithPromiseMiddleware($app->make('shared.kernel.handlers.locator.query'))
            ]);

            return new Foundation\Messenger\SymfonyQueryBus($bus);
        });

        $this->app->singleton(Foundation\Messenger\SymfonyCommandBus::class, function (Application $app) {
            $bus = new Foundation\Messenger\MessageBus([
                new Foundation\Messenger\Middleware\HandleMessageWithPromiseMiddleware($app->make('shared.kernel.handlers.locator.command'))
            ]);

            return new Foundation\Messenger\SymfonyCommandBus($bus);
        });

        $this->app->singleton(Foundation\Bus\QueryBus::class, Foundation\Messenger\SymfonyQueryBus::class);
        $this->app->singleton(Foundation\Bus\CommandBus::class, Foundation\Messenger\SymfonyCommandBus::class);

        $this->app->singleton(Foundation\Gateways\QueryGateway::class, SymfonyMessengerQueryGateway::class);
        $this->app->singleton(Foundation\Gateways\CommandGateway::class, Foundation\Gateways\SymfonyMessengerCommandGateway::class);

        parent::register();
    }
}
