<?php

namespace SharedKernel\Foundation\Gateways;

use React\Promise\Promise;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use SharedKernel\Foundation\Messenger\SymfonyCommandBus;

/**
 * Class SymfonyMessengerCommandGateway
 * @package SharedKernel\Foundation\Gateways
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
class SymfonyMessengerCommandGateway implements CommandGateway
{
    /** @var SymfonyCommandBus */
    private $commandBus;

    /**
     * DefaultCommandGateway constructor.
     * @param SymfonyCommandBus $commandBus
     */
    public function __construct(SymfonyCommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /** @inheritdoc */
    public function send($command)
    {
        /** @var Envelope $envelope */
        $envelope = $this->commandBus->dispatch($command);

        /** @var HandledStamp $lastStamp */
        $lastStamp = $envelope->last(HandledStamp::class);

        $response = null;

        if ($lastStamp instanceof HandledStamp) {
            $promise = $lastStamp->getResult();

            if ($promise instanceof Promise) {
                $promise->done(function ($result) use (&$response) {
                    $response = $result;
                }, function ($exception) {
                    throw $exception;
                });
            }
        }

        return $response;
    }
}
