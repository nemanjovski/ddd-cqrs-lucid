<?php

namespace SharedKernel\Foundation\Gateways;

/**
 * Interface QueryGateway
 * @package SharedKernel\Foundation\Gateways
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
interface QueryGateway
{
    /**
     * Put the query to the bus and return the result, the result depends on data and can't be specified here
     * @param $query
     * @return mixed
     */
    public function query($query);
}
