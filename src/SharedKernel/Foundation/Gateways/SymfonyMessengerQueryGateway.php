<?php

namespace SharedKernel\Foundation\Gateways;

use React\Promise\Promise;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use SharedKernel\Foundation\Messenger\SymfonyQueryBus;

/**
 * Class SymfonyMessengerQueryGateway
 * @package SharedKernel\Foundation\Gateways
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
class SymfonyMessengerQueryGateway implements QueryGateway
{
    /** @var SymfonyQueryBus */
    private $queryBus;

    /**
     * DefaultQueryGateway constructor.
     * @param SymfonyQueryBus $queryBus
     */
    public function __construct(SymfonyQueryBus $queryBus)
    {
        $this->queryBus = $queryBus;
    }

    /** @inheritdoc */
    public function query($query)
    {
        /** @var Envelope $envelope */
        $envelope = $this->queryBus->dispatch($query);

        /** @var HandledStamp $lastStamp */
        $lastStamp = $envelope->last(HandledStamp::class);

        $response = null;

        if ($lastStamp instanceof HandledStamp) {
            $promise = $lastStamp->getResult();
            if ($promise instanceof Promise) {
                $promise->done(function($result) use (&$response) {
                    $response = $result;
                }, function($exception) {
                    throw $exception;
                });
            }
        }

        return $response;
    }
}
