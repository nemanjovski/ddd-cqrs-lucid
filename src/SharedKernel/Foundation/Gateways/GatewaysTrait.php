<?php

namespace SharedKernel\Foundation\Gateways;

/**
 * Trait GatewaysTrait
 * @package SharedKernel\Foundation\Gateways
 */
trait GatewaysTrait
{
    /** @return CommandGateway */
    private function getCommandGateway(): CommandGateway
    {
        return \app(CommandGateway::class);
    }

    /** @return QueryGateway */
    private function getQueryGateway(): QueryGateway
    {
        return \app(QueryGateway::class);
    }

    /**
     * Execute query
     * @param $query
     * @return mixed
     */
    private function query($query)
    {
        return $this->getQueryGateway()->query($query);
    }

    /**
     * Execute command
     * @param $command
     * @return mixed
     */
    private function execute($command)
    {
        return $this->getCommandGateway()->send($command);
    }
}
