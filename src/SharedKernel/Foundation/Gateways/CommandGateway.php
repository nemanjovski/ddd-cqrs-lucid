<?php

namespace SharedKernel\Foundation\Gateways;

/**
 * Interface CommandGateway
 * @package SharedKernel\Foundation\Gateways
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
interface CommandGateway
{
    /**
     * Send the command to the bus
     *
     * @param mixed $command any command compatible with Command abstract class from framework
     * @return mixed
     */
    public function send($command);
}
