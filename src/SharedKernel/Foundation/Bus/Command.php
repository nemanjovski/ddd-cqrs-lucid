<?php

namespace SharedKernel\Foundation\Bus;

/**
 * Class Command
 * @package SharedKernel\Foundation\Bus
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
abstract class Command
{
    /**
     * @var array
     */
    protected $payload;

    public function __construct(array $payload = [])
    {
        $this->setPayload($payload);
    }

    public static function fromArray(array $payload)
    {
        return new static($payload);
    }

    public function payload(): array
    {
        return $this->payload;
    }

    protected function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }
}
