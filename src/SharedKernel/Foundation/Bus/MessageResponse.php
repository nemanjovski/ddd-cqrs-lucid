<?php

namespace SharedKernel\Foundation\Bus;

/**
 * Interface MessageResponse
 * @package SharedKernel\Foundation\Bus
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
interface MessageResponse
{
    /**
     * Put result in response
     *
     * @param $result
     * @return mixed depends on implementation
     */
    public function resolve($result);

    /**
     * Get response
     *
     * @return mixed depends on implementation
     */
    public function response();
}
