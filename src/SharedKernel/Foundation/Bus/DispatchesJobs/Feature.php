<?php

namespace SharedKernel\Foundation\Bus\DispatchesJobs;

use Lucid\Foundation\Feature as VendorFeature;

/**
 * Class Feature
 * @package SharedKernel\Foundation\Bus\DispatchesJobs
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
class Feature extends VendorFeature {}
