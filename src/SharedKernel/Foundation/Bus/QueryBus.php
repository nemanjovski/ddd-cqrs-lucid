<?php

namespace SharedKernel\Foundation\Bus;

/**
 * Interface QueryBus
 * @package SharedKernel\Foundation\Bus
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
interface QueryBus
{
    /**
     * Dispatch the message to the query bus
     * @param mixed $message the message can be whatever you want
     * @return mixed
     */
    public function dispatch($message);
}
