<?php

namespace SharedKernel\Foundation\Bus;

/**
 * Interface CommandBus
 * @package SharedKernel\Foundation\Bus
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
interface CommandBus
{
    /**
     * Dispatch message to the command bus
     *
     * @param mixed $message The message can be anything you want
     * @return mixed
     */
    public function dispatch($message);
}
