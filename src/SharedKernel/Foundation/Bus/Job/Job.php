<?php

namespace SharedKernel\Foundation\Bus\Job;

/**
 * Class Job
 * @package SharedKernel\Foundation\Bus\Job
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
class Job extends \Lucid\Foundation\Job {}
