<?php

namespace SharedKernel\Foundation\Bus\Job;

use Illuminate\Routing\ResponseFactory;

/**
 * Class RespondWithJsonErrorJob
 * @package SharedKernel\Foundation\Bus\Job
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
class RespondWithJsonErrorJob extends RespondWithJsonJob
{
    /**
     * RespondWithJsonErrorJob constructor.
     * @param string $message
     * @param int $code
     * @param array $headers
     * @param int $options
     */
    public function __construct($message = 'An error occurred', $code = 400, $headers = [], $options = 0)
    {
        $this->content = [
            'response' => [
                'code' => $code,
                'message' => $message,
            ],
        ];

        $this->status = $code;
        $this->headers = $headers;
        $this->options = $options;
    }

    /**
     * @param ResponseFactory $response
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(ResponseFactory $response)
    {
        return $response->json($this->content, $this->status, $this->headers, $this->options);
    }
}
