<?php

namespace SharedKernel\Foundation\Bus\Router;

/**
 * Interface CommandRouter
 * @package SharedKernel\Foundation\Bus\Router
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
interface CommandRouter extends HandlersRouter {}
