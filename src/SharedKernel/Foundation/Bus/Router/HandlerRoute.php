<?php

namespace SharedKernel\Foundation\Bus\Router;

/**
 * Class HandlerRoute
 * @package SharedKernel\Foundation\Bus\Router
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
class HandlerRoute
{
    /** @var string */
    private $name;
    private $to;

    /**
     * HandlerRoute constructor.
     * @param $name
     * @param null|mixed $to
     */
    public function __construct(string $name, $to = null)
    {
        $this->name = $name;
        $this->to = $to;
    }

    /** @return string */
    public function getName()
    {
        return $this->name;
    }

    /** @return mixed */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param $to
     * @return $this
     */
    public function to($to)
    {
        $this->to = $to;

        return $this;
    }
}
