<?php

namespace SharedKernel\Foundation\Bus\Router;

/**
 * Interface QueryRouter
 * @package SharedKernel\Foundation\Bus\Router
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
interface QueryRouter extends HandlersRouter {}
