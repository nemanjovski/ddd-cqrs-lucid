<?php

namespace SharedKernel\Foundation\Bus\Router;

/**
 * Interface HandlersRouter
 * @package SharedKernel\Foundation\Bus\Router
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
interface HandlersRouter
{
    /**
     * Get all handlers
     * @return array
     */
    public function all(): array;

    /**
     * Get all handlers by name
     *
     * @param string $name
     * @return array
     */
    public function allOf(string $name): array;

    /**
     * New route
     * @param string $name new route name
     * @return HandlerRoute
     */
    public function route(string $name): HandlerRoute;
}
