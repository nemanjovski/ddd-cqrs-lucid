<?php

namespace SharedKernel\Foundation\Messenger;

/**
 * Class MessageBus
 * @package SharedKernel\Foundation\Messenger
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
class MessageBus extends \Symfony\Component\Messenger\MessageBus {}
