<?php

namespace SharedKernel\Foundation\Messenger;

use SharedKernel\Foundation\Bus\CommandBus;

/**
 * Class SymfonyCommandBus
 * @package SharedKernel\Foundation\Messenger
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
class SymfonyCommandBus implements CommandBus
{
    /** @var MessageBus */
    private $messageBus;

    /**
     * SymfonyCommandBus constructor.
     * @param MessageBus $messageBus
     */
    public function __construct(MessageBus $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /** @inheritdoc */
    public function dispatch($message)
    {
        return $this->messageBus->dispatch($message);
    }
}
