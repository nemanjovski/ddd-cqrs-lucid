<?php

namespace SharedKernel\Foundation\Messenger\Handler;

use Psr\Container\ContainerInterface;
use Symfony\Component\Messenger\Envelope;
use SharedKernel\Foundation\Bus\Router\HandlersRouter;
use Symfony\Component\Messenger\Handler\HandlersLocatorInterface;

/**
 * Class ContainerAwareHandlersLocator
 * @package SharedKernel\Foundation\Messenger\Handler
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
class ContainerAwareHandlersLocator implements HandlersLocatorInterface
{
    /** @var $handlersRouter */
    private $handlersRouter;
    /** @var ContainerInterface */
    private $container;

    /**
     * @param ContainerInterface $container
     * @param HandlersRouter $handlersRouter
     */
    public function __construct(ContainerInterface $container, HandlersRouter $handlersRouter)
    {
        $this->handlersRouter = $handlersRouter;
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function getHandlers(Envelope $envelope): iterable
    {
        $seen = array();

        foreach (self::listTypes($envelope) as $type) {
            foreach ($this->handlersRouter->allOf($type) ?? array() as $alias => $handler) {
                if (!\in_array($handler, $seen, true)) {
                    yield $alias => $seen[] = $this->container->get($handler);
                }
            }
        }
    }

    /**
     * @internal
     * @param Envelope $envelope
     * @return array
     */
    private static function listTypes(Envelope $envelope): array
    {
        $class = \get_class($envelope->getMessage());

        return array($class => $class)
            + class_parents($class)
            + class_implements($class)
            + array('*' => '*');
    }
}
