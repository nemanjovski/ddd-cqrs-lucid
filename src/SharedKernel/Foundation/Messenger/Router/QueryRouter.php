<?php

namespace SharedKernel\Foundation\Messenger\Router;

use SharedKernel\Foundation\Bus\Router\HandlerRoute;
use SharedKernel\Foundation\Bus\Router\HandlersRouter as HandlersRouterContract;

/**
 * Class QueryRouter
 * @package SharedKernel\Foundation\Messenger\Router
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
class QueryRouter implements HandlersRouterContract
{
    /** @var HandlersRouter */
    private $handlersRouter;

    /**
     * QueryRouter constructor.
     * @param HandlersRouter $handlersRouter
     */
    public function __construct(HandlersRouter $handlersRouter)
    {
        $this->handlersRouter = $handlersRouter;
    }

    /** @inheritdoc */
    public function all(): array
    {
        return $this->handlersRouter->all();
    }

    /** @inheritdoc */
    public function allOf(string $name): array
    {
        return $this->handlersRouter->allOf($name);
    }

    /** @inheritdoc */
    public function route(string $name): HandlerRoute
    {
        return $this->handlersRouter->route($name);
    }
}
