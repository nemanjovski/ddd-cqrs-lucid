<?php

namespace SharedKernel\Foundation\Messenger\Router;

use SharedKernel\Foundation\Bus\Router\HandlerRoute;
use SharedKernel\Foundation\Bus\Router\HandlersRouter as HandlersRouterContract;

/**
 * Class HandlersRouter
 * @package SharedKernel\Foundation\Messenger\Router
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
class HandlersRouter implements HandlersRouterContract
{
    /** @var array */
    private $handlers;

    /** @inheritdoc */
    public function all(): array
    {
        return \collect($this->handlers)->map(function($handler) {
            return $handler->getTo();
        })->toArray();
    }

    /** @inheritdoc */
    public function allOf(string $name): array
    {
        return \collect($this->all())->filter(function($handler, $key) use ($name) {
            return $name === $key;
        })->toArray();
    }

    /** @inheritdoc */
    public function route(string $name): HandlerRoute
    {
        return $this->handlers[$name] = new HandlerRoute($name);
    }
}
