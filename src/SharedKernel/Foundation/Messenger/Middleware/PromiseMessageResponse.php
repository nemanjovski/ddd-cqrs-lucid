<?php

namespace SharedKernel\Foundation\Messenger\Middleware;

use React\Promise\Deferred;
use SharedKernel\Foundation\Bus\MessageResponse;

/**
 * Class PromiseMessageResponse
 * @package SharedKernel\Foundation\Messenger\Middleware
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
class PromiseMessageResponse implements MessageResponse
{
    /** @var Deferred */
    private $promise;

    /**
     * PromiseQueryResponse constructor.
     * @param Deferred $promise
     */
    public function __construct(Deferred $promise)
    {
        $this->promise = $promise;
    }

    /** @inheritdoc */
    public function resolve($result)
    {
        return $this->promise->resolve($result);
    }

    /** @inheritdoc */
    public function response()
    {
        return $this->promise->promise();
    }
}
