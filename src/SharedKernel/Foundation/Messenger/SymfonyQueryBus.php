<?php

namespace SharedKernel\Foundation\Messenger;

use SharedKernel\Foundation\Bus\QueryBus;

/**
 * Class SymfonyQueryBus
 * @package SharedKernel\Foundation\Messenger
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
class SymfonyQueryBus implements QueryBus
{
    /** @var MessageBus */
    private $messageBus;

    /**
     * SymfonyQueryBus constructor.
     * @param MessageBus $messageBus
     */
    public function __construct(MessageBus $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /** @inheritdoc */
    public function dispatch($message)
    {
        return $this->messageBus->dispatch($message);
    }
}
