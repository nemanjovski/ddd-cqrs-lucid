<?php

namespace SharedKernel\Application;

use Illuminate\Support\Facades\Event;
use SharedKernel\Foundation\Bus\Router\QueryRouter;
use SharedKernel\Foundation\Bus\Router\CommandRouter;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

/**
 * Class AbstractApplicationServiceProvider
 * @package SharedKernel\Application
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
abstract class AbstractApplicationServiceProvider extends ServiceProvider
{
    /** @var array */
    protected $listeners = [
        //event class => listener class
    ];
    /** @var array */
    protected $queries = [
        // Query:class => Handler::class
    ];
    /** @var array  */
    protected $commands = [
        // Command:class => Handler::class
    ];

    /**
     * @var array
     * @see https://laravel.com/docs/5.5/events#registering-event-subscribers
     */
    protected $subscribe = [
        // 'App\Listeners\UserEventSubscriber',
    ];

    /** @inheritdoc */
    public function register()
    {
        /** @var QueryRouter $queryRouter */
        $queryRouter = $this->app->make(QueryRouter::class);

        /** @var CommandRouter $commandRouter */
        $commandRouter = $this->app->make(CommandRouter::class);

        foreach ($this->queries as $query => $handler) {
            $this->app->singleton($handler);
            $queryRouter->route($query)->to($handler);
        }

        foreach ($this->commands as $command => $handler) {
            $this->app->singleton($handler);
            $commandRouter->route($command)->to($handler);
        }

        foreach ($this->listeners as $event => $listeners) {
            foreach ($listeners as $listener) {
                Event::listen($event, $listener);
            }
        }

        foreach ($this->subscribe as $subscriber) {
            Event::subscribe($subscriber);
        }
    }
}
