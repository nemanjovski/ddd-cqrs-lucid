<?php

namespace SharedKernel\Application\Presenters;

use Illuminate\Support\Facades\Storage;

/**
 * Class PersonAvatarPresenter
 * @package SharedKernel\Application\Presenters
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
final class PersonAvatarPresenter
{
    /** @var int|null */
    private $personId;

    /**
     * PersonAvatarPresenter constructor.
     * @param int|null $personId
     */
    public function __construct(?int $personId)
    {
        $this->personId = $personId;
    }

    /** @return string */
    protected function getSuggestedAvatarPath(): string
    {
        if (is_int($this->personId)) {
            return 'images/avatars/' . $this->personId . '.jpg';
        }

        return $this->getDefaultAvatarPath();
    }

    /** @return string */
    public function getDefaultAvatarPath(): string
    {
        return 'images/default_hem_avatar.png';
    }

    /**
     * Get person photo
     *
     * @return string
     */
    public function getAvatarPath(): string
    {
        $filePath = $this->getSuggestedAvatarPath();

        if (Storage::disk('public')->exists($filePath)) {
            return $filePath;
        }

        return $this->getDefaultAvatarPath();
    }

    /**
     * Get person photo
     *
     * @return string
     */
    public function getAvatarLocalFullPath(): string
    {
        $filePath = $this->getSuggestedAvatarPath();

        if (Storage::disk('public')->exists($filePath)) {
            $storagePath  = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();

            return \sprintf("%s%s", $storagePath, $filePath);
        }

        return $this->getDefaultAvatarPath();
    }

    /**
     * Get person photo
     *
     * @return string
     */
    public function getAvatarUrl(): string
    {
        $filePath = $this->getSuggestedAvatarPath();

        if (Storage::disk('public')->exists($filePath)) {
            return \avatar_url(
                $filePath,
                \env('APP_HTTPS_ENABLE', false)
            );
        }

        return \asset($this->getDefaultAvatarPath(), \env('APP_HTTPS_ENABLE', false));
    }
}
