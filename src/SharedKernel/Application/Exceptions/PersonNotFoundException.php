<?php

namespace SharedKernel\Application\Exceptions;

/**
 * Class PersonNotFoundException
 * @package SharedKernel\Application\Exceptions
 */
class PersonNotFoundException extends \Exception
{
    public function __construct(
        string $message = "",
        int $code = 0,
        \Throwable $previous = null
    ) {
        if ($message === "") {
            $message = 'Person not found';
        }

        parent::__construct($message, $code, $previous);
    }
}
