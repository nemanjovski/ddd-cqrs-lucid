<?php

declare(strict_types=1);

namespace SharedKernel\Common\Messaging;

/**
 * Trait PayloadTrait
 * @package SharedKernel\Common\Messaging
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
trait PayloadTrait
{
    /**
     * @var array
     */
    protected $payload;

    /**
     * PayloadTrait constructor.
     * @param array $payload
     */
    public function __construct(array $payload = [])
    {
        $this->setPayload($payload);
    }

    /**
     * @param array $payload
     * @return self
     */
    public static function fromArray(array $payload)
    {
        return new static($payload);
    }

    /** @return array */
    public function payload(): array
    {
        return $this->payload;
    }

    /** @param array $payload */
    protected function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }
}
