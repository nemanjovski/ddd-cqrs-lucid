<?php

namespace SharedKernel\Common\Dto;

use Carbon\Carbon;
use Illuminate\Support\Str;

/**
 * Class Dto
 * @package SDC\SharedKernel\Common\Dto
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
abstract class Dto
{
    /**
     * Convert to date string
     *
     * @param $dateString
     * @return string
     */
    protected function toDateString($dateString): ?string
    {
        if (is_null($dateString)) {
            return null;
        }
        return Carbon::parse($dateString)->toDateString();
    }

    /**
     * Assert value as date string or null
     * @param $dateStr
     */
    protected function assertValidDateStringOrNull($dateStr)
    {
        if (is_null($dateStr)) {
            return;
        }

        if (!is_string($dateStr)) {
            throw new \InvalidArgumentException('Value should be null or string with valid date');
        }

        if (\strtotime($dateStr) === -1) {
            throw new \InvalidArgumentException('Value should be null or string with valid date');
        }
    }

    /**
     * Get values from payload
     * @param array $payload
     * @return void
     */
    protected function getValuesFromPayload(array $payload): void
    {
        foreach (array_keys($payload) as $key) {
            if (isset($payload[$key])) {
                $method = 'set' . Str::camel($key);
                if (method_exists($this, $method)) {
                    call_user_func([$this, $method], $payload[$key]);
                }
            }
        }
    }
}

