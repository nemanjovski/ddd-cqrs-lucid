<?php

namespace Patterns;

use Exception;
use Patterns\CreationalDesignPatterns\Factory\CustomerQueryInterface;
use Patterns\CreationalDesignPatterns\Factory\Genres\ActionMovie;
use Patterns\CreationalDesignPatterns\Factory\Genres\ComedyMovie;
use Patterns\CreationalDesignPatterns\Factory\movies\action\Rocky;
use Patterns\CreationalDesignPatterns\Factory\movies\Comedy\MontyPython;
use Patterns\CreationalDesignPatterns\Factory\QueryInterface;
use Patterns\CreationalDesignPatterns\Factory\CustomerQuery;
use SharedKernel\Container\Traits\AggregateIoC;

trait IoCServiceProvider
{
    use AggregateIoC;

    public function register(Object $abstract)
    {
        $bindings =  [
            ComedyMovie::class => MontyPython::class,
            ActionMovie::class => Rocky::class,
        ];

        try {
            return $this->build($bindings, $abstract);
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
}

