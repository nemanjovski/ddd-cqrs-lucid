<?php

namespace Patterns;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Patterns\CreationalDesignPatterns\Builder\MySqlQueryBuilder;
use Patterns\CreationalDesignPatterns\Builder\SqlQueryBuilder;

class ApplicationServiceProvider extends ServiceProvider
{

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();
    }

    public function boot()
    {
        parent::boot();

        $this->loadMorph();
    }

    protected function loadMorph() {}

    /**
     * Register application
     */
    public function register()
    {
        parent::register();

        $this->app->bind(SqlQueryBuilder::class, MySqlQueryBuilder::class);
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'prefix' => 'patterns',
        ], function () {

            Route::group([
                'namespace' => 'Patterns\CreationalDesignPatterns\Factory\Http\Controller',
            ], function () {
                require __DIR__
                    . '/CreationalDesignPatterns/Factory/Http/routes.php';
            });

            Route::group([
                'namespace' => 'Patterns\CreationalDesignPatterns\AbstractFactory\Http\Controller',
            ], function () {
                require __DIR__
                    . '/CreationalDesignPatterns/AbstractFactory/Http/routes.php';
            });

            Route::group([
                'namespace' => 'Patterns\CreationalDesignPatterns\Builder\Http\Controller',
            ], function () {
                require __DIR__
                    . '/CreationalDesignPatterns/Builder/Http/routes.php';
            });
        });
    }
}

