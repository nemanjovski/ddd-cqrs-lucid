<?php

namespace Patterns\CreationalDesignPatterns\AbstractFactory\context;

use Patterns\CreationalDesignPatterns\AbstractFactory\BaseShowContext;

class StandUpComedyShowContext extends BaseShowContext
{
    public function getShowContextString(): string
    {
        $renderedTitle = $this->titleTemplate->getTitleString();

        return $renderedTitle
            . ' overview --->>> Stand Up comedy stands for a live show in front of audience, which is commonly run by solo entertainer.';
    }
}

