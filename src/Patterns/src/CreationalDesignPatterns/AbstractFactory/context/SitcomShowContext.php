<?php

namespace Patterns\CreationalDesignPatterns\AbstractFactory\context;

use Patterns\CreationalDesignPatterns\AbstractFactory\BaseShowContext;

class SitcomShowContext extends BaseShowContext
{
    public function getShowContextString(): string
    {
        $renderedTitle = $this->titleTemplate->getTitleString();

        return $renderedTitle
            . ' overview --->>> Sitcom is a played serial, which could contain multiple episodes, and sometimes even a seasons!';
    }
}

