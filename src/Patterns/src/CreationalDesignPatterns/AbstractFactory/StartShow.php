<?php

namespace Patterns\CreationalDesignPatterns\AbstractFactory;

interface StartShow
{
    public function getStartShow(string $templateString, string $context, array $arguments = []): string;
}
