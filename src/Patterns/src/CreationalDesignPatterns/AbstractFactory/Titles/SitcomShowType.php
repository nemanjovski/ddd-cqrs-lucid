<?php

namespace Patterns\CreationalDesignPatterns\AbstractFactory\Titles;

use Patterns\CreationalDesignPatterns\AbstractFactory\ShowType;

class SitcomShowType implements ShowType
{
    public function getTitleString(): string
    {
        return 'Sitcom';
    }
}
