<?php

namespace Patterns\CreationalDesignPatterns\AbstractFactory\Titles;

use Patterns\CreationalDesignPatterns\AbstractFactory\ShowType;

class StandUpComedyShowType implements ShowType
{
    public function getTitleString(): string
    {
        return 'Stand Up';
    }
}
