<?php

namespace Patterns\CreationalDesignPatterns\AbstractFactory\Http\Controller;

use App\Http\Controllers\Controller;
use Patterns\CreationalDesignPatterns\AbstractFactory\Netflix;
use Patterns\CreationalDesignPatterns\AbstractFactory\Shows\SitcomShowFactory;
use Patterns\CreationalDesignPatterns\AbstractFactory\Shows\StandUpComedyShowFactory;

class ExecuteAbstractFactoryController extends Controller
{
    public function __invoke()
    {
        $netflix = new Netflix('Only Fools and Horses', 'Comedy');

//        $netflix = new Netflix('Eddie Murphy - Against all odds', 'Comedy');

//        echo $netflix->render(new StandUpComedyShowFactory());

        echo $netflix->render(new SitcomShowFactory);
    }
}
