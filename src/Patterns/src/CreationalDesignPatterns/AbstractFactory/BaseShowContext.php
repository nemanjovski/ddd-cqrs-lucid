<?php

namespace Patterns\CreationalDesignPatterns\AbstractFactory;


abstract class BaseShowContext implements ShowContext
{
    protected $titleTemplate;

    public function __construct(ShowType $titleTemplate)
    {
        $this->titleTemplate = $titleTemplate;
    }
}
