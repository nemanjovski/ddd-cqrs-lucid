<?php

namespace Patterns\CreationalDesignPatterns\AbstractFactory\Shows;

use Patterns\CreationalDesignPatterns\AbstractFactory\context\SitcomShowContext;
use Patterns\CreationalDesignPatterns\AbstractFactory\ShowStarters\SitcomShowStarter;
use Patterns\CreationalDesignPatterns\AbstractFactory\ShowType;
use Patterns\CreationalDesignPatterns\AbstractFactory\ShowContext;
use Patterns\CreationalDesignPatterns\AbstractFactory\EntertainmentFactory;
use Patterns\CreationalDesignPatterns\AbstractFactory\StartShow;
use Patterns\CreationalDesignPatterns\AbstractFactory\Titles\SitcomShowType;


/**
 * And this Concrete Factory creates Sitcom shows templates.
 */
class SitcomShowFactory implements EntertainmentFactory
{
    public function showType(): ShowType
    {
        return new SitcomShowType();
    }

    public function showContext(): ShowContext
    {
        return new SitcomShowContext($this->showType());
    }

    public function watchShow(): StartShow
    {
        return new SitcomShowStarter();
    }
}

