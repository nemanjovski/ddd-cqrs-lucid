<?php

namespace Patterns\CreationalDesignPatterns\AbstractFactory\Shows;

use Patterns\CreationalDesignPatterns\AbstractFactory\ShowType;
use Patterns\CreationalDesignPatterns\AbstractFactory\StartShow;
use Patterns\CreationalDesignPatterns\AbstractFactory\ShowContext;
use Patterns\CreationalDesignPatterns\AbstractFactory\EntertainmentFactory;
use Patterns\CreationalDesignPatterns\AbstractFactory\Titles\StandUpComedyShowType;
use Patterns\CreationalDesignPatterns\AbstractFactory\context\StandUpComedyShowContext;
use Patterns\CreationalDesignPatterns\AbstractFactory\ShowStarters\StandUpComedyShowStarter;


/**
 * Each Concrete Factory corresponds to a specific variant (or family) of
 * products.
 *
 * This Concrete Factory creates EddieMurphy's show template.
 */
class StandUpComedyShowFactory implements EntertainmentFactory
{
    public function showType(): ShowType
    {
        return new StandUpComedyShowType();
    }

    public function showContext(): ShowContext
    {
        return new StandUpComedyShowContext($this->showType());
    }

    public function watchShow(): StartShow
    {
        return new StandUpComedyShowStarter();
    }
}

