<?php

namespace Patterns\CreationalDesignPatterns\AbstractFactory;

interface ShowContext
{
    public function getShowContextString(): string;
}
