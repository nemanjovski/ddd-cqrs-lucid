<?php

namespace Patterns\CreationalDesignPatterns\AbstractFactory;

/**
 * The Abstract Factory interface declares creation methods for each distinct
 * product type.
 */
interface EntertainmentFactory
{
    public function showType(): ShowType;

    public function showContext(): ShowContext;

    public function watchShow(): StartShow;
}

