<?php

namespace Patterns\CreationalDesignPatterns\AbstractFactory;

class Netflix
{
    public $title;

    public $genre;

    public function __construct($title, $genre)
    {
        $this->title = $title;
        $this->genre = $genre;
    }

    public function render(EntertainmentFactory $factory): string
    {
        $type = $factory->showType();

        $context = $factory->showContext();

        $renderer = $factory->watchShow();

        return $renderer->getStartShow($type->getTitleString(), $context->getShowContextString(), [
            'title' => $this->title,
            'genre' => $this->genre
        ]);
    }
}

