<?php

namespace Patterns\CreationalDesignPatterns\AbstractFactory\ShowStarters;

use Patterns\CreationalDesignPatterns\AbstractFactory\StartShow;

class SitcomShowStarter implements StartShow
{
    public function getStartShow(string $templateString, string $context, array $arguments = []): string
    {
        return 'Selected show from Netflix you have chosen is the '. $arguments['genre']
            . ' genre  and type of ' . $templateString . ' called ' . $arguments['title']
            .'. '. $context;
    }
}

