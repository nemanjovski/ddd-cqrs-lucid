<?php

namespace Patterns\CreationalDesignPatterns\Builder\Http\Controller;

use App\Http\Controllers\Controller;
use Patterns\CreationalDesignPatterns\Builder\SqlQueryBuilder;
use PhpParser\Node\Scalar\String_;

class BuilderExecuteController extends Controller
{
    public function __invoke(SqlQueryBuilder $queryBuilder)
    {
        return $queryBuilder
            ->select("users", ["name", "email", "password"])
            ->where("age", 18, ">")
            ->where("age", 30, "<")
            ->limit(10, 20)
            ->getSqlQuery()
        ;
    }

}

