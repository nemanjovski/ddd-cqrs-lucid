Factory method is a creational design pattern which solves the problem of creating product objects without specifying their concrete classes.

Factory Method defines a method, which should be used for creating objects instead of direct constructor call (new operator). Subclasses can override this method to change the class of objects that will be created.

Conceptual Example

This example illustrates the structure of the Factory Method design pattern and focuses on the following questions:

What classes does it consist of?
What roles do these classes play?
In what way the elements of the pattern are related?
After learning about the pattern’s structure it’ll be easier for you to grasp the following example, based on a real-world PHP use case.
===============================================================================

Real World Example

In this example, the Factory Method pattern provides an interface for creating social network connectors, which can be used to log in to the network, create posts and potentially perform other activities—and all of this without coupling the client code to specific classes of the particular social network.
