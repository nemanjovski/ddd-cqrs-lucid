<?php

namespace Patterns\CreationalDesignPatterns\Factory\movies\Comedy;

use Patterns\CreationalDesignPatterns\Factory\Genre;

class MontyPython implements Genre
{

    public function play(): string
    {
        return 'Monty Python starts now...';
    }
}
