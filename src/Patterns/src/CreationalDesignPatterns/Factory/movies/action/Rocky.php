<?php

namespace Patterns\CreationalDesignPatterns\Factory\movies\action;

use Patterns\CreationalDesignPatterns\Factory\Genre;

class Rocky implements Genre
{

    public function play(): string
    {
        return 'Rocky movie stars now...';
    }
}
