<?php

namespace Patterns\CreationalDesignPatterns\Factory\Genres;

use Patterns\CreationalDesignPatterns\Factory\Movie;
use Patterns\CreationalDesignPatterns\Factory\Genre;
use Patterns\CreationalDesignPatterns\Factory\movies\Comedy\MontyPython;

class ComedyMovie extends Movie
{
    public function genre(): Genre
    {
        return new MontyPython;
    }
}

