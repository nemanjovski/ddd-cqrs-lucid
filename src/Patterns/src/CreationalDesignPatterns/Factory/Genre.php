<?php

namespace Patterns\CreationalDesignPatterns\Factory;

interface Genre
{
    public function play(): string ;
}
