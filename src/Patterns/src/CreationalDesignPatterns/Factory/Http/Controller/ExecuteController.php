<?php

namespace Patterns\CreationalDesignPatterns\Factory\Http\Controller;

use Patterns\IoCServiceProvider;
use App\Http\Controllers\Controller;
use Patterns\CreationalDesignPatterns\Factory\Genres\ComedyMovie;

class ExecuteController extends Controller
{
    use IoCServiceProvider;

    protected $film;

    /**
     * ExecuteController constructor.
     * @param $film
     */
    public function __construct(ComedyMovie $film)
    {
        $this->film = $this->register($film);
    }

    public function __invoke()
    {
          return $this->film->play();
    }
}

