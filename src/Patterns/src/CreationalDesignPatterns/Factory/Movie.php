<?php

namespace Patterns\CreationalDesignPatterns\Factory;

abstract class Movie
{
    abstract public function genre(): Genre;

    public function playMovie(): string
    {
        return $this->genre()->play();
    }
}

