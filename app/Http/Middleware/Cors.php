<?php

namespace App\Http\Middleware;

/**
 * Class Cors
 * @package App\Http\Middleware
 * @author Nemanja Milosavljevic <Nemanja.Milosavljevic@hemmersbach.com>
 */
class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $nextStep = $next($request);

        if (method_exists($nextStep, 'header')) {
            return $nextStep
                ->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS, PATCH');
        }

        return $nextStep;
    }
}
