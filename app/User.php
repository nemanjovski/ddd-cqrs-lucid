<?php

namespace App;

use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use HasApiTokens;
    use SoftDeletes;

    const TABLE_NAME = 'users';
    const ID_COLUMN = 'id';
    const COMMENT_NAME = 'name';
    const COMMENT_EMAIL = 'email';
    const COMMENT_PASSWORD = 'password';

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $table = self::TABLE_NAME;

    protected $dateFormat = 'Y-m-d H:i:s.u';


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->getKey();
    }

    /**
     * @return null|string
     */
    public function getName(): ?string
    {
        return $this->getAttribute(self::COMMENT_NAME);
    }

    /**
     * @return null|string
     */
    public function getEmail(): ?string
    {
        return $this->getAttribute(self::COMMENT_EMAIL);
    }

    /**
     * @return null|string
     */
    public function getPassword(): ?string
    {
        return $this->getAttribute(self::COMMENT_PASSWORD);
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User
     */
    public static function createUser(
        string $name,
        string $email,
        string $password
    ): User
    {
        $instance = new static;
        $instance->name = $name;
        $instance->email = $email;
        $instance->password = Hash::make($password);
        $instance->api_token = $instance->createToken('auth_token')->accessToken;

        $instance->save();

        return $instance;
    }
}

